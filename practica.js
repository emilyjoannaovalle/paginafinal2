document.addEventListener("DOMContentLoaded", function() {
    var practicasBtn = document.getElementById("practicasBtn");
    var practicasMenu = document.getElementById("practicasMenu");

    practicasBtn.addEventListener("click", function() {
        practicasMenu.classList.toggle("show");
    });
});


const imageContainers = document.querySelectorAll('.image-container');

imageContainers.forEach(container => {
  container.addEventListener('mouseenter', () => {
    container.style.transform = 'scale(1.05)';
    container.style.transition = 'transform 0.3s ease';
  });

  container.addEventListener('mouseleave', () => {
    container.style.transform = 'scale(1)';
    container.style.transition = 'transform 0.3s ease';
  });
});
